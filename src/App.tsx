import * as React from "react";
import { Provider } from "react-redux";
import { store } from "./store";

import { hot } from "react-hot-loader";
import TaskPage from "./screens/TaskPage";

class App extends React.Component<Record<string, unknown>, undefined> {
  public render() {
    return (
      <Provider store={store}>
        <div className="app">
          <TaskPage />
        </div>
      </Provider>
    );
  }
}

declare let module: Record<string, unknown>;

export default hot(module)(App);
