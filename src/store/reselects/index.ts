import { createSelector } from "reselect";
import { ITask, ITaskState } from "../actions-type";

const taskSelector = (state: any) => state.tasks;

export const dataSelector = createSelector(
  [taskSelector],
  (taskState: ITaskState): ITask[] => {
    const tmpData = [];
    Object.values(taskState.dataTask).forEach((el: ITask) => {
      tmpData.push({ ...el, key: el.id });
    });
    return tmpData;
  },
);

export const activeArraySelector = createSelector(
  [taskSelector],
  (taskState: ITaskState) => {
    const tmpData: number[] = [];
    Object.values(taskState.dataTask).forEach((el: ITask) => {
      el.isActive && tmpData.push(el.id);
    });
    return tmpData;
  },
);

//For Form
export const dataFormSelector = createSelector(
  [taskSelector],
  (taskState: ITaskState) => {
    return taskState.dataForm.data;
  },
);

export const typeFormSelector = createSelector(
  [taskSelector],
  (taskState: ITaskState) => {
    return taskState.dataForm.type;
  },
);

export const fetchDataFormSelector = createSelector(
  [taskSelector],
  (taskState: ITaskState) => {
    return taskState.fetchState;
  },
);

export const showDialogSelector = createSelector(
  [taskSelector],
  (taskState: ITaskState): boolean => {
    return taskState.showDialog;
  },
);
