import { Dispatch } from "redux";

import {
  addForm,
  addTaskCreate,
  clearForm,
  completedTaskCreate,
  completedTasksCreate,
  delTaskCreate,
  editTaskCreate,
  setTypeForm,
  showDiagForm,
} from "../actions";

import {
  ActionType,
  IAddToFormAction,
  IEmptyFormAction,
  ISetTypeTaskAction,
  IShowFormAction,
  ITask,
  TypeFormAction,
} from "../actions-type";

export interface IAddTaskCreateAction {
  type: ActionType;
  payload: ITask;
}
export interface IEditTaskCreate {
  type: ActionType;
  payload: ITask;
}

export interface IDelTaskCreate {
  type: ActionType;
  payload: number;
}

export interface ICompletedTaskCreate {
  type: ActionType;
  payload: number;
}
export interface ICompletedTasksCreate {
  type: ActionType;
  payload: number[];
}

export const addTaskWith =
  (task: ITask) => (dispatch: Dispatch<IAddTaskCreateAction>) => {
    return dispatch(addTaskCreate(task));
  };

export const editTask =
  (task: ITask) => (dispatch: Dispatch<IEditTaskCreate>) => {
    return dispatch(editTaskCreate(task));
  };

export const setTypeTaskForm =
  (type: TypeFormAction) => (dispatch: Dispatch<ISetTypeTaskAction>) => {
    return dispatch(setTypeForm(type));
  };

export const delTask = (id: number) => (dispatch: Dispatch<IDelTaskCreate>) => {
  return dispatch(delTaskCreate(id));
};

export const completedTask =
  (id: number) => (dispatch: Dispatch<ICompletedTaskCreate>) => {
    return dispatch(completedTaskCreate(id));
  };

export const completedTasks =
  (ids: number[]) => (dispatch: Dispatch<ICompletedTasksCreate>) => {
    return dispatch(completedTasksCreate(ids));
  };

// For Form
export const addTaskToForm =
  (task: ITask) => (dispatch: Dispatch<IAddToFormAction>) => {
    return dispatch(addForm(task));
  };

export const clearTaskForm = () => (dispatch: Dispatch<IEmptyFormAction>) => {
  return dispatch(clearForm());
};

export const showTaskForm =
  (type: boolean) => (dispatch: Dispatch<IShowFormAction>) => {
    console.log("showTaskForm 33", type);
    return dispatch(showDiagForm(type));
  };
