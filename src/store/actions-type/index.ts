export interface ITask {
  id?: number;
  title: string;
  description: string;
  isActive: boolean;
  startDate: string;
  [propName: string]: any; // for antd need key
}

export type TypeFormAction = "save" | "edit" | "none";

export interface IDataForm {
  data: ITask;
  title: string;
  type: TypeFormAction;
}

export interface IFetchData {
  loading: boolean;
  messageError: string;
}

export interface ITaskState {
  dataTask: {
    [key: string]: ITask;
  };
  dataForm: IDataForm;
  fetchState: IFetchData;
  showDialog: boolean;
}

export enum ActionType {
  ADD_TASK = "ADD_TASK",
  DELETE_TASK = "DELETE_TASK",
  EDIT_TASK = "EDIT_TASK",
  COMPLETED_TASK = "COMPLETED_TASK",
  COMPLETED_TASKS = "COMPLETED_TASKS",
}

export interface IAddTaskAction {
  type: ActionType.ADD_TASK;
  payload: ITask;
}

export interface IDeleteTaskAction {
  type: ActionType.DELETE_TASK;
  payload: number;
}

export interface IEditTaskAction {
  type: ActionType.EDIT_TASK;
  payload: ITask;
}

export interface ICompletedTaskAction {
  type: ActionType.COMPLETED_TASK;
  payload: number; // id
}
export interface ISetTypeTaskAction {
  type: ActionForm.SET_TYPE_TASK_FORM;
  payload: TypeFormAction; // "save" | "edit"
}
// for change tab completed
export interface ICompletedTasksAction {
  type: ActionType.COMPLETED_TASKS;
  payload: number[]; // ids
}

/// For Form Dialog
export enum ActionForm {
  ADD_TASK_FORM = "ADD_TASK_FORM",
  EMPTY_TASK_FORM = "EMPTY_TASK_FORM",
  SET_TYPE_TASK_FORM = "SET_TYPE_TASK_FORM",
  SHOW_TASK_DIALOG = "SHOW_TASK_DIALOG",
}

export interface ISetTypeTaskForm {
  type: ActionForm.SET_TYPE_TASK_FORM;
  payload: TypeFormAction;
}

export interface IAddToFormAction {
  type: ActionForm.ADD_TASK_FORM;
  payload: ITask;
}

export interface IEmptyFormAction {
  type: ActionForm.EMPTY_TASK_FORM;
}

export interface IShowFormAction {
  type: ActionForm.SHOW_TASK_DIALOG;
  payload: boolean; // open || close
}

export interface IStateForm {
  id: number;
  title: string;
  description: string;
  isActive: boolean;
  startDate: string;
  isModalVisible: boolean;
  [propName: string]: any; // for antd need key
}
export interface IEmptyForm {
  id: number;
  title: string;
  description: string;
  isActive: boolean;
  startDate: string;
}

export type TAction =
  | IAddTaskAction
  | IDeleteTaskAction
  | IEditTaskAction
  | ISetTypeTaskForm
  | ICompletedTaskAction
  | ICompletedTasksAction
  | IAddToFormAction
  | IEmptyFormAction
  | IShowFormAction;
