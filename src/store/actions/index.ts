import {
  ActionType,
  ActionForm,
  IAddTaskAction,
  IAddToFormAction,
  ICompletedTaskAction,
  IDeleteTaskAction,
  IEditTaskAction,
  IEmptyFormAction,
  ITask,
  TypeFormAction,
  IShowFormAction,
  ICompletedTasksAction,
  ISetTypeTaskAction,
} from "../actions-type";

export const addTaskCreate = (task: ITask): IAddTaskAction => {
  return { type: ActionType.ADD_TASK, payload: task };
};

export const editTaskCreate = (task: ITask): IEditTaskAction => {
  return { type: ActionType.EDIT_TASK, payload: task };
};

export const delTaskCreate = (id: number): IDeleteTaskAction => {
  return { type: ActionType.DELETE_TASK, payload: id };
};

export const completedTaskCreate = (id: number): ICompletedTaskAction => {
  return { type: ActionType.COMPLETED_TASK, payload: id };
};

export const completedTasksCreate = (ids: number[]): ICompletedTasksAction => {
  return { type: ActionType.COMPLETED_TASKS, payload: ids };
};

//For Dialog

export const setTypeForm = (type: TypeFormAction): ISetTypeTaskAction => {
  return { type: ActionForm.SET_TYPE_TASK_FORM, payload: type };
};

export const addForm = (task: ITask): IAddToFormAction => {
  return { type: ActionForm.ADD_TASK_FORM, payload: task };
};

export const clearForm = (): IEmptyFormAction => {
  return { type: ActionForm.EMPTY_TASK_FORM };
};

export const showDiagForm = (show: boolean): IShowFormAction => {
  return { type: ActionForm.SHOW_TASK_DIALOG, payload: show };
};
