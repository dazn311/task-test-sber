import produce from "immer";
import UUId from "lodash/uniqueId";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import moment from "moment";

import {
  ActionType,
  ActionForm,
  ITaskState,
  TAction,
  IEmptyForm,
  TypeFormAction,
} from "../actions-type";
// import * as moment from "moment";
// import { v4 as uuIdv4 } from 'uuid';

export const emptyForm: IEmptyForm = {
  id: 0,
  title: "",
  description: "",
  isActive: false,
  startDate: moment().format("L"),
};

const initState: ITaskState = {
  dataTask: {
    "0": {
      id: 0,
      title: "Kelvin",
      description: "What I do…",
      isActive: false,
      startDate: "23.04.2021",
    },
    "1": {
      id: 1,
      title: "Alex",
      description: "Fly to Sochi…",
      isActive: false,
      startDate: "25.04.2021",
    },
  },
  dataForm: {
    data: emptyForm,
    title: "",
    type: "edit", // "save" | "edit" | "none";
  },
  fetchState: {
    loading: false,
    messageError: "",
  },
  showDialog: false,
};

export const reducerTasks = produce(
  (state: ITaskState = initState, action: TAction) => {
    switch (action.type) {
      case ActionType.ADD_TASK:
        const newId: string = UUId();
        console.log("ADD_TASK11 newId", newId);
        if (state.dataTask[newId]) {
          console.log("error ADD_TASK11: rr");
          state.fetchState.messageError = "Error: we already have such a task";
          return;
        } else {
          state.fetchState.messageError = "";
        }
        state.dataTask[newId] = { ...action.payload, id: parseInt(newId) };
        return;
      case ActionType.EDIT_TASK:
        const { id: idEdit } = action.payload;
        state.dataTask[`${idEdit}`] = action.payload;
        return;
      case ActionType.DELETE_TASK:
        const idToDel: number = action.payload;
        delete state.dataTask[idToDel];
        return;
      case ActionType.COMPLETED_TASK:
        state.dataTask[`${action.payload}`].isActive =
          !state.dataTask[`${action.payload}`].isActive;
        return;
      case ActionType.COMPLETED_TASKS:
        const ids: number[] = action.payload;
        // console.log("11 redux ids", ids);
        Object.keys(state.dataTask).forEach((id) => {
          ids.includes(parseInt(id))
            ? (state.dataTask[`${id}`].isActive = true)
            : (state.dataTask[`${id}`].isActive = false);
        });
        return;
      case ActionForm.ADD_TASK_FORM:
        state.dataForm.data = action.payload;
        return;
      case ActionForm.EMPTY_TASK_FORM:
        state.dataForm.data = emptyForm;
        return;
      case ActionForm.SET_TYPE_TASK_FORM:
        const typeAct = action.payload;
        if (typeAct === "save" || typeAct === "edit" || typeAct === "none") {
          state.dataForm.type = action.payload as TypeFormAction;
        }
        return;
      case ActionForm.SHOW_TASK_DIALOG:
        state.showDialog = action.payload;
        return;
      default:
        return state;
    }
  },
);

export default reducerTasks;
