import { combineReducers } from "redux";

import reducerTasks from "./taskReducer";

const reducers = combineReducers({
  tasks: reducerTasks,
});

export default reducers;

export type RootState = ReturnType<typeof reducers>;
