import { combineReducers } from "redux";
// import { persistReducer  } from 'redux-persist';
// import storage from 'redux-persist/lib/storage';
// import sessionStorage from 'redux-persist/lib/storage/session';

import reducerTasks from "./reducers/taskReducer";

// sessionStorage.clear();
// const persistConfig = {
//   key: 'root',
//   storage,
//   // storage: sessionStorage,
//   whitelist: ['adminPanel,obj']
// }

export const rootReducer = combineReducers({
  tasks: reducerTasks,
});

// export default persistReducer(persistConfig, rootReducer);
