import * as React from "react";

import { Breadcrumb, Layout, Menu } from "antd";
const { Header, Content, Footer } = Layout;
import "antd/dist/antd.css";

import TaskListTab from "../components/Task/TaskListTab";
import ModalTask from "../components/Modal";

export const TaskPage: React.FC = () => {
  return (
    <div>
      <Layout>
        <Header style={{ position: "fixed", zIndex: 1, width: "100%" }}>
          <div className="logo" />
          <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["2"]}>
            {/*<Menu.Item key="1">Home</Menu.Item>*/}
            <Menu.Item key="2">Tasks</Menu.Item>
          </Menu>
        </Header>
        <Content
          className="site-layout"
          style={{ padding: "0 50px", marginTop: 64 }}
        >
          <Breadcrumb style={{ margin: "16px 0" }}>
            <Breadcrumb.Item>Home</Breadcrumb.Item>
            <Breadcrumb.Item>Tasks</Breadcrumb.Item>
          </Breadcrumb>
          <div
            className="site-layout-background"
            style={{ padding: 24, minHeight: 380 }}
          >
            <div style={{ display: "flex", flexDirection: "column" }} />
            <ModalTask />
            <TaskListTab />
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Ant Design ©2018 Created by Ant UED
        </Footer>
      </Layout>
    </div>
  );
};

export default TaskPage;
