import * as React from "react";
import { connect } from "react-redux";
import { ThunkDispatch } from "@reduxjs/toolkit";
import { createStructuredSelector } from "reselect";

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import moment from "moment";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
// import "antd/dist/antd.css";
import { Button, Modal } from "antd";
import "./Modal.styles.css";

import {
  ISetTypeTaskAction,
  IStateForm,
  ITask,
  ITaskState,
  TAction,
  TypeFormAction,
} from "../../store/actions-type";

import {
  clearTaskForm,
  addTaskWith,
  editTask,
  setTypeTaskForm,
  showTaskForm,
} from "../../store/action-creaters";

import {
  dataFormSelector,
  showDialogSelector,
  typeFormSelector,
} from "../../store/reselects";
import FormValidate from "./FormValidate/FormValidate";

const initState: IStateForm = {
  id: 0,
  title: "",
  description: "",
  isActive: false,
  startDate: moment().format("L"),
  isModalVisible: false,
  typeAction: "none",
};
interface ModalTaskProps {
  dataForm?: IStateForm;
  datatypeForm?: TypeFormAction;
  isShowDialog: boolean;
  addTaskWith: (task: {
    description: string;
    title: string;
    isActive: boolean;
    startDate: string;
  }) => void;
  clearTaskForm: () => void;
  editTask: (task: {
    id: number;
    description: string;
    title: string;
    isActive: boolean;
    startDate: string;
  }) => void;
  setTypeTask: (task: TypeFormAction) => void;
  showDiag: (show: boolean) => void;
}

// eslint-disable-next-line react/display-name
const ModalTask: React.FC<ModalTaskProps> = React.memo(
  ({
    dataForm,
    datatypeForm,
    isShowDialog,
    addTaskWith,
    clearTaskForm,
    editTask,
    setTypeTask,
    showDiag,
  }: ModalTaskProps) => {
    const [values, setValues] = React.useState<IStateForm>(() => {
      return dataForm ? dataForm : initState;
    });

    React.useEffect(() => {
      setValues({
        id: dataForm.id,
        title: dataForm.title,
        description: dataForm.description,
        startDate: dataForm.startDate, // .toLocaleString().slice(0, 10),
        isActive: dataForm.isActive,
        isModalVisible: isShowDialog,
        typeAction: datatypeForm,
      });
    }, [isShowDialog, dataForm]);

    const resetStateModal = () => {
      const nVal = {
        ...values,
        title: "",
        description: "",
        isActive: false,
        isModalVisible: false,
        startDate: moment().format("L"),
        typeAction: "none",
      };
      setValues(nVal);
    };

    const handlerOpenModal = () => {
      setValues({
        ...values,
        typeAction: "save",
      });
      cleanForm();
      showDiag(true);
      setTypeTask("save");
    };

    const cleanForm = () => {
      clearTaskForm();
      setTypeTask("none");
      resetStateModal();
    };

    const closeDialog = () => {
      showDiag(false);
      clearTaskForm();
      setTypeTask("none");
      resetStateModal();
    };

    const onFinish = (fieldsValue: ITask) => {
      if (values.typeAction === "edit") {
        editTask({
          id: fieldsValue.id,
          title: fieldsValue.title,
          description: fieldsValue.description,
          isActive: fieldsValue.isActive,
          startDate: fieldsValue.startDate,
        });
      } else if (values.typeAction === "save") {
        addTaskWith({
          title: fieldsValue.title,
          description: fieldsValue.description,
          isActive: fieldsValue.isActive,
          startDate: fieldsValue.startDate,
        });
      }
      cleanForm();
      showDiag(false);
    };

    return (
      <>
        <Button className="modal" type="primary" onClick={handlerOpenModal}>
          Add Task
        </Button>
        {values.isModalVisible && (
          <Modal
            title={datatypeForm === "edit" ? "Edit Task" : "Add Task"}
            visible={values.isModalVisible}
            footer={null}
            // onOk={handleOk}
            onCancel={closeDialog}
          >
            <FormValidate
              handleSubmit={onFinish}
              values={values}
              closeDialog={closeDialog}
              // handleChange={handleChange}
              // handleDatePicker={handleDatePicker}
            />
          </Modal>
        )}
      </>
    );
  },
);

const mapDispatchToProps = (
  dispatch: ThunkDispatch<ITaskState, TAction, ISetTypeTaskAction>,
) => {
  return {
    addTaskWith: (task: ITask) => {
      dispatch(addTaskWith(task));
    },
    clearTaskForm: () => {
      dispatch(clearTaskForm());
    },
    editTask: (task: ITask) => {
      dispatch(editTask(task));
    },
    setTypeTask: (type: TypeFormAction) => {
      dispatch(setTypeTaskForm(type));
    },
    showDiag: (show: boolean) => {
      dispatch(showTaskForm(show));
    },
  };
};

const mapStateToProps = createStructuredSelector({
  dataForm: dataFormSelector,
  datatypeForm: typeFormSelector,
  isShowDialog: showDialogSelector,
});

export const ModalT = connect(mapStateToProps, mapDispatchToProps)(ModalTask);

export default ModalT;
