import * as React from "react";
import { ChangeEvent } from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import * as moment from "moment";

import { Button, DatePicker, Form, Input } from "antd";
import { ValidateStatus } from "antd/lib/form/FormItem";
import "./FormValidate.styles.css";
const dateFormatList = ["DD.MM.YYYY", "DD/MM/YY"];

import { dataSelector } from "../../../store/reselects";
import { IStateForm, ITask, TypeFormAction } from "../../../store/actions-type";

interface ResultValidateFunc {
  validateStatus: ValidateStatus;
  errorMsg: string;
}

function validatePrimeNumber(
  fieldTaskValue: string,
  tasksArr: ITask[],
): ResultValidateFunc {
  if (fieldTaskValue.length > 2) {
    const isHasTask = tasksArr.findIndex((el) => el.title === fieldTaskValue);
    if (isHasTask >= 0) {
      return {
        validateStatus: "error",
        errorMsg: "there is already such a task",
      };
    }
    return {
      validateStatus: "success",
      errorMsg: "",
    };
  }

  return {
    validateStatus: "error",
    errorMsg: `The title need between 3 and 22 is ${fieldTaskValue.length}`,
  };
}

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 4,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 20,
    },
  },
};

interface RowFormInputProps {
  dataTaskArr: [ITask];
  values: IStateForm;
  closeDialog: () => void;
  handleSubmit: (prop: {
    description: string;
    title: string;
    isActive: boolean;
    startDate: string;
  }) => void;
  // handleDatePicker: (date: moment.MomentInput, dateString: string) => void;
}

interface RawFormProps {
  title: string;
  description: string;
  date: string;
  validateStatus: ValidateStatus;
  errorMsg: string;
  typeAction: TypeFormAction;
}
//
// TODO: RowForm
//
// eslint-disable-next-line react/prop-types
const RawForm = ({
  dataTaskArr,
  values,
  closeDialog,
  handleSubmit,
}: RowFormInputProps) => {
  const [fieldTaskValue, setFieldTaskValue] = React.useState<RawFormProps>({
    title: values.title ? values.title : "",
    description: values.description ? values.description : "",
    date: values.startDate ? values.startDate : moment().format("DD.MM.YYYY"),
    validateStatus: "",
    errorMsg: "",
    typeAction: values.typeAction,
  });
  const tips = "New task enter, pls.";

  React.useEffect(() => {
    setFieldTaskValue({
      ...fieldTaskValue,
      title: values.title,
      description: values.description,
      date: values.startDate ? values.startDate : moment().format("DD.MM.YYYY"),
      typeAction: values.typeAction,
    });
  }, [values]);

  const onFinish = (fieldsValue) => {
    const titleValue = fieldsValue["title-task"];
    const descValue = fieldsValue["description-task"];
    const dateValue = fieldsValue["date-task"]
      ? fieldsValue["date-task"].format("DD.MM.YYYY")
      : moment().format("DD.MM.YYYY");
    handleSubmit({
      title: titleValue,
      description: descValue,
      isActive: false,
      startDate: dateValue,
    });
  };

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const handleDatePicker = (date: moment, dateString: string) => {
    setFieldTaskValue({ ...fieldTaskValue, date: dateString });
  };

  const onDescChange = React.useCallback(
    (prop: keyof ITask) => (event: ChangeEvent<HTMLInputElement>) => {
      setFieldTaskValue({
        ...fieldTaskValue,
        ...validatePrimeNumber(event.target.value, dataTaskArr),
        [prop]: event.target.value,
      });
    },
    [setFieldTaskValue, dataTaskArr, fieldTaskValue],
  );

  return (
    <Form
      onFinish={onFinish}
      fields={[
        {
          name: ["title-task"],
          value: fieldTaskValue.title,
        },
        {
          name: ["description-task"],
          value: fieldTaskValue.description,
        },
      ]}
    >
      <Form.Item
        {...formItemLayout}
        label="Title"
        name="title-task"
        validateStatus={fieldTaskValue.validateStatus}
        help={fieldTaskValue.errorMsg || tips}
      >
        <Input value={fieldTaskValue.title} onChange={onDescChange("title")} />
      </Form.Item>
      <Form.Item {...formItemLayout} label="Desc" name="description-task">
        <Input
          value={fieldTaskValue.description}
          onChange={onDescChange("description")}
        />
      </Form.Item>
      <Form.Item
        wrapperCol={{
          xs: {
            span: 24,
            offset: 0,
          },
          sm: {
            span: 20,
            offset: 4,
          },
        }}
        name="date-task"
      >
        <DatePicker
          mode="date"
          placeholder={fieldTaskValue.date}
          // defaultPickerValue={moment(fieldTaskValue.date)}
          value={moment(values.startDate.slice(0, 10), dateFormatList[0])}
          format={dateFormatList[0]}
          onChange={handleDatePicker}
        />
      </Form.Item>
      <Form.Item
        wrapperCol={{
          xs: {
            span: 24,
            offset: 0,
          },
          sm: {
            span: 20,
            offset: 4,
          },
        }}
      >
        <Button
          type="primary"
          htmlType="submit"
          disabled={
            fieldTaskValue.errorMsg.length > 0 ||
            fieldTaskValue.title.length < 3
          }
        >
          Submit
        </Button>
        <Button onClick={closeDialog} type="dashed" htmlType="button">
          Cancel
        </Button>
      </Form.Item>
    </Form>
  );
};

const mapStateToProps = createStructuredSelector({
  dataTaskArr: dataSelector,
});

const _RawForm = connect(mapStateToProps)(RawForm);
export default _RawForm;

// export const maxLengthCreator = (maxLength) => (value) => {
//   if (value.length > maxLength) return `Max length is ${maxLength}`;
//   return undefined;
// };

// const maxLength7 = maxLengthCreator(7);

// const handleChange = React.useCallback(
//   (prop: keyof ITask) => (event: React.ChangeEvent<HTMLInputElement>) => {
//     setFieldTaskValue({ ...fieldTaskValue, [prop]: event.target.value });
//   },
//   [setFieldTaskValue],
// );

// const onTitleChange = React.useCallback(
//   (value: ChangeEvent<HTMLInputElement>) => {
//     setFieldTaskValue({
//       ...fieldTaskValue,
//       ...validatePrimeNumber(value.target.value, dataTaskArr),
//       value: value.target.value,
//     });
//     handleChange("title");
//   },
//   [setFieldTaskValue],
// );
