// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { createStructuredSelector } from "reselect";

import { Table } from "antd";

import {
  ITask,
  TAction,
  ITaskState,
  TypeFormAction,
  IEmptyForm,
  IFetchData,
} from "../../store/actions-type";
import {
  dataSelector,
  dataFormSelector,
  fetchDataFormSelector,
  activeArraySelector,
} from "../../store/reselects";
import {
  completedTask,
  completedTasks,
  delTask,
  showTaskForm,
  addTaskToForm,
  setTypeTaskForm,
} from "../../store/action-creaters";

const columns = [
  {
    key: "title",
    title: "title",
    dataIndex: "title",
  },
  {
    key: "description",
    title: "description",
    dataIndex: "description",
  },
  {
    key: "startDate",
    title: "start Date",
    dataIndex: "startDate",
  },
  // {
  //   key: "action",
  //   title: "Edit",
  //   dataIndex: "action",
  // eslint-disable-next-line react/display-name
  // render: (action: any, record) => (
  //   // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  //   // @ts-ignore
  //   <Button onClick={() => selectRowHandler(record)}>Edit</Button>
  // ),
  // },
];

type ItemsListProps = {
  dataTaskArr: ITask[];
  // tasks: { [key: string]: ITask };
  completedArray: (ids: number[]) => void;
  completed: (id: number) => void;
  delTask: (id: number) => void;
  setTypeTask: (type: TypeFormAction) => void;
  addTaskDataToForm: (task: ITask) => void;
  showDiag: (show: boolean) => void;
  dataFormS?: IEmptyForm;
  fetchDataForm?: IFetchData;
  activeArray?: number[];
};

type TState = {
  data: ITask[]; // Check here to configure the default column
  selectedRowKeys: number[]; // Check here to configure the default column
  dataForm: IEmptyForm;
  showDialog: boolean;
};

//
//
// TODO: Task List
//
class TaskListTab extends PureComponent<ItemsListProps, TState> {
  constructor(props: ItemsListProps) {
    super(props);
    this.state = {
      data: [], // Check here to configure the default column
      selectedRowKeys: [], // Check here to configure the default column
      dataForm: {
        id: 0,
        title: "",
        description: "",
        isActive: false,
        startDate: "",
      },
      showDialog: false,
    };
  }

  componentDidMount(): void {
    if (this.props.dataTaskArr) {
      this.updateTask(this.props.dataTaskArr);
    }
  }

  // shouldComponentUpdate(nextProps) {
  //   console.log("should");
  //   if (this.props.dataFormS !== nextProps.dataFormS) {
  //     return false;
  //   }
  //   return true;
  // }

  componentDidUpdate(prevProps: ItemsListProps): void {
    if (this.props.dataTaskArr !== prevProps.dataTaskArr) {
      this.updateTask(this.props.dataTaskArr);
    }
    if (this.props.dataFormS !== prevProps.dataFormS) {
      this.setState({
        ...this.state,
        dataForm: this.props.dataFormS,
      });
    }
    if (this.props.activeArray !== prevProps.activeArray) {
      this.setState({
        ...this.state,
        selectedRowKeys: this.props.activeArray,
      });
    }
  }

  updateTask(dataTaskArr): void {
    this.setState({ ...this.state, data: dataTaskArr });
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  onChange = (ids: number[]): void => {
    const { completedArray } = this.props;
    ids.length && completedArray(ids);
  };

  selectRowHandler = (task): void => {
    const { showDiag, addTaskDataToForm, setTypeTask } = this.props;
    setTypeTask("edit");
    addTaskDataToForm({
      id: task.id,
      title: task.title ? task.title : "",
      description: task.description ? task.description : "",
      isActive: task.isActive,
      startDate: task.startDate,
    });
    showDiag(true);
  };

  render() {
    const dataTask = this.props.dataTaskArr;
    const { selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onChange,
      selections: [
        Table.SELECTION_ALL,
        Table.SELECTION_INVERT,
        Table.SELECTION_NONE,
        {
          key: "odd",
          text: "Select Odd Row",
          onSelect: (changableRowKeys) => {
            let newSelectedRowKeys = [];
            newSelectedRowKeys = changableRowKeys.filter((key, index) => {
              return index % 2 === 0;
            });
            this.setState({ selectedRowKeys: newSelectedRowKeys });
          },
        },
        {
          key: "even",
          text: "Select Even Row",
          onSelect: (changableRowKeys) => {
            let newSelectedRowKeys = [];
            newSelectedRowKeys = changableRowKeys.filter((key, index) => {
              return index % 2 !== 0;
            });
            this.setState({ selectedRowKeys: newSelectedRowKeys });
          },
        },
      ],
    };
    return (
      <Table
        rowSelection={rowSelection}
        columns={columns}
        dataSource={dataTask}
        onRow={(record) => ({
          onClick: () => {
            this.selectRowHandler(record);
          },
        })}
      />
    );
  }
}

const mapStateToProps = createStructuredSelector({
  dataTaskArr: dataSelector,
  dataFormS: dataFormSelector,
  fetchDataForm: fetchDataFormSelector,
  activeArray: activeArraySelector,
});

const mapDispatchToProps = (
  dispatch: ThunkDispatch<ITaskState, TAction, any>,
) => {
  return {
    completedArray: (ids: number[]) => {
      dispatch(completedTasks(ids));
    },
    completed: (id: number) => {
      dispatch(completedTask(id));
    },
    delTask: (id: number) => {
      dispatch(delTask(id));
    },
    setTypeTask: (type: TypeFormAction) => {
      dispatch(setTypeTaskForm(type));
    },
    addTaskDataToForm: (task: ITask) => {
      dispatch(addTaskToForm(task));
    },
    showDiag: (show: boolean) => {
      dispatch(showTaskForm(show));
    },
  };
};

const TaskList = connect(mapStateToProps, mapDispatchToProps)(TaskListTab);
export default TaskList;
